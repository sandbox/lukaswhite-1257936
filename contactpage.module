<?php 

/**
 * Implementation of hook_menu().
 */
function contactpage_menu() {
  $items = array();

	if (variable_get('contactpage_page', false)) {
		$items[variable_get('contactpage_page_path', 'contact')] = array(
	    'access arguments' => array('access content'),
	    'page callback' => 'contactpage_page',
	    'type' => MENU_CALLBACK,
	  );	
	}
  
  $items['admin/settings/contactpage'] = array(
    'title' => 'Contact Page Settings',
    'description' => 'Settings for Contact Page module',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('contactpage_admin_settings_form'),	    
    'access arguments' => array('administer site configuration'),
    'file' => 'contactpage.admin.inc',
  );
  
  $items['admin/content/contactpage'] = array(
    'title' => 'Contact Page',
    'description' => 'Contact Page information',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('contactpage_address_form'),	    
    'access arguments' => array('administer site configuration'),
    'file' => 'contactpage.admin.inc',
  );
  
  return $items;
}

/**
 * Implementation of hook_theme()
 */
function contactpage_theme() {
  return array(
	  'contactpage_map' => array(
	    'arguments' => array('location' => NULL),
	  ),
	  'contactpage_page' => array(
	    'arguments' => array('location' => NULL),
	    'path' => drupal_get_path('module', 'contactpage') . '/theme',
	    'template' => 'contactpage-page',
	  ),
	  'contactpage_address' => array(
	 		'arguments' => array('location' => NULL),
	  )
  );
}

/**
 * Implementation of hook_block()
 */
function contactpage_block($op = 'list', $delta = 0, $edit = array()) {
  global $user;

  if ($op == 'list') {
    
  	$blocks[0]['info'] = t('Contactpage Address');    
    $blocks[0]['cache'] = BLOCK_NO_CACHE;

    $blocks[1]['info'] = t('Contactpage Map');
    $blocks[1]['cache'] = BLOCK_NO_CACHE;
    
    return $blocks;
  }
  
  else if ($op == 'view') {

  	$block = array();

    switch ($delta) {
      case 0:

		    $contactpage_locations = location_load_locations('contactpage_location', 'genid');
		    if (count($contactpage_locations)) {
		
		    	// only one location, but this will be an array
		    	$location = count($contactpage_locations) ? $contactpage_locations[0] : array();	
		    	
		    	$block['content'] =  theme('contactpage_address', $location);
		    	
		    } else {
		    	// no location has been specified, throw up an error
		    	$block['content'] = '<p>'.t('Please specify the address in the <a href="/admin/content/contactpage">configuration</a>').'</p>';
		    }
	      	
	      return $block;
        
      case 1:
      	
      	$contactpage_locations = location_load_locations('contactpage_location', 'genid');
		    if (count($contactpage_locations)) {
		
		    	// only one location, but this will be an array
		    	$location = count($contactpage_locations) ? $contactpage_locations[0] : array();	
		    	
		    	$block['content'] =  theme('contactpage_map', $location);
		    	
		    } else {
		    	// no location has been specified, throw up an error
		    	$block['content'] = '<p>'.t('Please specify the address in the <a href="/admin/content/contactpage">configuration</a>').'</p>';
		    }
      	
        return $block;  
        
    }
    
  }
  
}

/**
 * Page callback
 */
function contactpage_page()
{
	// get locations
	$contactpage_locations = location_load_locations('contactpage_location', 'genid');
  if (count($contactpage_locations)) {

  	// only one location, but this will be an array
  	$location = count($contactpage_locations) ? $contactpage_locations[0] : array();	
    	var_dump($location);
  	return theme('contactpage_page', $location);
    	
  } else {
  	// no location has been specified, throw up an error
  	return '<p>'.t('Please specify the address in the <a href="/admin/content/contactpage">configuration</a>').'</p>';    
  }
	  
}

/**
 * Adds the Javascript to make the map work
 */
function _contactpage_map_js($location)
{
	// include the Google Maps JS; note that it needs to tell Google whether a sensor is being used
	$url = sprintf('http://maps.google.com/maps/api/js?sensor=%s', ((variable_get('contactpage_map_sensor', false))?'true':'false'));
	drupal_set_html_head('<script type="text/javascript" src="'.$url.'"></script>');	
	
	$address_html = $location['name'].'<br />'.$location['street'];
	
	$js1 = '
	function contactpage_map_initialize() {
	
		var homeLatlng = new google.maps.LatLng('.$location['latitude'].', '.$location['longitude'].');
	
	    var mapOptions = {
	      zoom: 12,
	      center: homeLatlng,
	      mapTypeId: google.maps.MapTypeId.ROADMAP,
	      streetViewControl: false,
	      mapTypeControl: false,
	    };
	    var map = new google.maps.Map(document.getElementById("contactpage_map_canvas"),
	        mapOptions);
	        
	    var marker = new google.maps.Marker({
	      position: homeLatlng, 
	      map: map, 
	      title: "'.$location['name'].'"
	    });  
		var infowindow = new google.maps.InfoWindow({
		    content: "'.$address_html.'"
		});
		
		infowindow.open(map,marker);
	  
	        
	  }
	';
	
	drupal_add_js($js1, 'inline');
	
	$js2 = 'Drupal.behaviors.contactpage = function (context) {
		contactpage_map_initialize();
	}';
	
	drupal_add_js($js2, 'inline');

}

/**
 * Implementation of hook_locationapi().
 */
function contactpage_locationapi(&$obj, $op, $a3 = NULL, $a4 = NULL, $a5 = NULL) {
  	
  if ($op=='defaults') {

		// Return sensible defaults for the location settings
  	
  	/**
  	 The collect values, for reference, are:
		 0 - do not collect
  	 1 - allow
  	 2 - require
  	 3 - force default
  	 */
  		
		return array(
      'lid'         => array('default' => FALSE),
      'name'        => array('default' => '', 'collect' => 1, 'weight' => 2),
      'street'      => array('default' => '', 'collect' => 2, 'weight' => 4),
      'additional'  => array('default' => '', 'collect' => 1, 'weight' => 6),
      'city'        => array('default' => '', 'collect' => 2, 'weight' => 8),
      'province'    => array('default' => '', 'collect' => 0, 'weight' => 10),
      'postal_code' => array('default' => '', 'collect' => 2, 'weight' => 12),
      'country'     => array('default' => variable_get('location_default_country', 'uk'), 'collect' => 1, 'weight' => 14), // @@@ Fix weight?
      'locpick'     => array('default' => FALSE, 'collect' => 0, 'weight' => 20, 'nodiff' => TRUE),
      'latitude'    => array('default' => 0),
      'longitude'   => array('default' => 0),      
      'source'      => array('default' => LOCATION_LATLON_JIT_GEOCODING),
      'is_primary'  => array('default' => 0), // @@@
      'delete_location' => array('default' => FALSE, 'nodiff' => TRUE),
    );
  	
  }
}
 
/** THEME FUNCTIONS **/

/**
 * Theme the address
 * 
 * @param array $location
 * @return string
 */
function theme_contactpage_address($location)
{
	$countries = location_get_iso3166_list();
	
	$address = '<div itemscope="itemscope" itemtype="http://data-vocabulary.org/Address">';
	if (strlen($location['name'])>0) {
		$address .= '<span class="contactpage-locationname">'.$location['name'].'</span><br />';
	}
	$address = '<span itemprop="street-address">'.$location['street'].'</span><br />';
	if (strlen($location['additional'])>0) {
		$address .= '<span itemprop="street-address">'.$location['additional'].'</span><br />';
	}
	$address .= '<span itemprop="locality">'.$location['city'].'</span><br />';
	if (strlen($location['province'])>0) {
		$address .= '<span itemprop="region">'.$location['province'].'</span><br />';
	}
	$address .= '<span itemprop="postal-code">'.$location['postal_code'].'</span><br />';
	$address .= '<span itemprop="country-name">'.$countries[strtolower($location['country'])].'</span>';
		
	return $address;
}

/**
 * Theme a Google Map
 * 
 * @param array $location
 * @return string
 */
function theme_contactpage_map($location)
{
	// if geocoding is not enabled, or no lat/lng specified, display a warning.
	if (($location['latitude']==0)||($location['longitude']==0)) {
		$output = t('The following map won\'t display correctly, because the latitude and / or longitude values are invalid. You can fix this by enabling geocoding in the locations module, or entering the latitude and longitude values manually. Note that you\'ll probably have to alter the location settings for this module to allow you to do so.');
	} else {
		$output = '';
	}
	
	// add the JS
	_contactpage_map_js($location);
	
	// render the containing DIV
	$output .= '<div id="contactpage_map_canvas" style="width:'.variable_get('contactpage_map_width', 320).'px; height:'.variable_get('contactpage_map_width', 320).'px;"></div>';
	return $output;	
}

/**
 * Implementation of hook_token_values()
 */
function contactpage_token_values($type, $object = NULL, $options = array()) {
  if ($type == 'global') {
  	
		// provide the telephone number as a token. However there are a number of situations
		// where we can't do this. First, the module may not be enabled. Second, there might
		// not yet be an address specified. Third, it might be blank and/or the user has not
		// opted to allow a telephone number. We just need to handle this as gracefully as we
		// can. We shouldn't really be in a situation where we're returning a notice that the
		// location_module must be enabled as we only define the token if that is the case;
		// howaver there is no harm in checking - particularly as tokens are heavily cached 		
    if (module_exists('location_phone')) {
    	$location = _contactpage_get_location();
			if ($location) {
				$tokens['telephone']     		= $location['phone'];
			} else {
				$tokens['telephone']      	= t('Please specify an address for the contact page');
			}
		} else {
			$tokens['telephone']      		= t('Please install the Location Phone module');
		}
    	
    if (module_exists('location_fax')) {
    	// we may have already returned the location, don't do that twice	
    	if (!$location) {
    		$location = _contactpage_get_location();
    	}    	
			if ($location) {
				$tokens['fax']      				= $location['fax'];
			} else {
				$tokens['fax']      				= t('Please specify an address for the contact page');
			}
		} else {
			$tokens['fax']      					= t('Please install the Location Fax module');
		}
		
    return $tokens;
  }
}

/**
 * Implementation of hook_token_list()
 */
function contactpage_token_list($type = 'all') {
  if ($type == 'global' || $type == 'all') {
  	if (module_exists('location_phone')) {
    	$tokens['contactpage']['telephone']     = t('The Contact Page telephone number');
		}
		if (module_exists('location_fax')) {    
			$tokens['contactpage']['fax']      			= t('The Contact Page fax number');
			}
    return $tokens;
  }
}

/**
 * Gets the defined location
 */
function _contactpage_get_location()
{
	$contactpage_locations = location_load_locations('contactpage_location', 'genid');
  if (count($contactpage_locations)) {
  	// only one location, but this will be an array
  	$location = count($contactpage_locations) ? $contactpage_locations[0] : array();
	} else {
		$location = null;
	}
	return $location;
}
