<?php 
/**
 * Alter the user_admin_settings form.
 */
function contactpage_admin_settings_form() {
  if (isset($form_state['values']['contactpage_location_settings'])) {
    $settings = $form_state['values']['contactpage_location_settings'];
  }
  else {
    $settings = variable_get('contactpage_location_settings', array());
  }

  $form['contactpage_location_settings'] = location_settings($settings);
  $form['contactpage_location_settings']['#title'] = t('Location Settings');
	//$form['contactpage_location_settings']['#collapsed'] = 0;
	$form['contactpage_location_settings']['#collapsible'] = 0;
	//$form['contactpage_location_settings']['form']['#collapsed'] = 0;
	$form['contactpage_location_settings']['form']['#collapsible'] = 0;
	// Ignore multiple (we're only using one location for now)
	unset($form['contactpage_location_settings']['multiple']);
	// Ignore the display settings, the address is fully themeable
	unset($form['contactpage_location_settings']['display']);

	// Page settings	
	$form['contactpage_page_settings'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('Page Settings'),
    '#collapsed'     => false,
    '#collapsible'   => true,
  );

	$form['contactpage_page_settings']['contactpage_page'] = array(
    '#type' => 'checkbox',
    '#title' => t('Create a page?'),
    '#default_value' => variable_get('contactpage_page', false),
  );

	$form['contactpage_page_settings']['contactpage_page_path'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Path'),
    '#prefix'				 =>	$_SERVER['SERVER_NAME'].'/',
    '#default_value' => variable_get('contactpage_page_path', 'contact'),
    '#size'          => 30,
    '#maxlength'     => 100,
    '#description'   => t('Specify the path to the contact page. You should ensure that this does not already exist.'),
  );

	// Map settings
	$form['contactpage_map_settings'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('Map Settings'),
    '#collapsed'     => false,
    '#collapsible'   => true,
  );
	
  $form['contactpage_map_settings']['contactpage_map_sensor'] = array(
    '#type' => 'checkbox',
    '#title' => t('Sensor setting'),
    '#default_value' => variable_get('contactpage_map_sensor', false),
    '#description'   => t('Google Maps requires that you specify whether you are using geolocation (i.e. the sensor parameter) - please set it here'),    
  );

	$form['contactpage_map_settings']['contactpage_map_width'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Width'),
    '#default_value' => variable_get('contactpage_map_width', 320),
    '#size'          => 5,
    '#maxlength'     => 5,
    '#description'   => t('Enter the width of the map, in pixels'),
  );
	
	$form['contactpage_map_settings']['contactpage_map_height'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Height'),
    '#default_value' => variable_get('contactpage_map_height', 280),
    '#size'          => 5,
    '#maxlength'     => 5,
    '#description'   => t('Enter the height of the map, in pixels'),
  );

	$form = system_settings_form($form);

	// we need to rebuild the menu cache after submission, so add
	// an additional submit handler
  $form['#submit'][] = 'contactpage_admin_settings_form_submit';

  return $form;

}

/**
 * Additional submit handler for the settings form
 * 
 * Since it's a system_settings_form, all the saving is handled for us.
 * However because of the page functionality, it becomes necessary to
 * rebuild the menu cache
 */
function contactpage_admin_settings_form_submit($form, &$form_state) {
	
	drupal_set_message('Additional form submit');
  if ($form_state['values']['contactpage_page']) {
  	// Update the menu router information.
  	drupal_set_message('Rebuilding menu');
		menu_rebuild();	
  }
  
}

/**
 * The address form
 */
function contactpage_address_form()
{	
	$contactpage_locations = location_load_locations('contactpage_location', 'genid');	
  $location = count($contactpage_locations) ? $contactpage_locations : array();
	
	$settings = variable_get('contactpage_location_settings', array());	
	
	// get the location form
	$form['locations'] = location_form($settings, $location);
	
	// expand it to tidy up
	$form['locations'][0]['#collapsible'] = 0;
	$form['locations'][0]['#collapsed'] = 0;
	
	$form['submit'] = array(
		'#type' => 'submit', 
		'#value' => t('Submit')
	);
	
	return $form;
		
}

/**
 * Form sumit handler, saves the address
 */
function contactpage_address_form_submit($form, &$form_state)
{	
	// locations = ?
	
	$locations = $form_state['values']['locations'];
		
	$criteria = array(
		'genid' => 'contactpage_location'
	);
	
	//print_r($locations);
	
	location_save_locations($locations, $criteria);
	
	drupal_set_message(t('Location saved'));
	
}
