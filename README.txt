------------
Contact Page
------------

The Contact Page module makes developing a contact page, which represents a physical location, that much easier.

Whilst the Location module (a requirement of this module) makes it possible to define physical locations, and offers
neat functionality such as geocoding, what it lacks is a simple way to associate a location with the entity a website
represents - whether it's a company, a restaurant, or a barbers shop.

This module allows you to edit the location of an organisation in Drupal from its own tab, and helps you
generate a contact page by:

- providing an optional, fully themeable contact page at a path of your choice
- provides a fully themeable representation of the address, using the latest HTML5 microdata format. This is also
provided as a block
- provides a simple, configurable Google Map
- exposes a telephone number and / or fax number (requires location_phone and/or location_fax) as tokens - define once,
use across the site

--------------------
Instructions for use
--------------------

1. Ensure you have the location module, along with (if required) the bundled location_phone and location_fax modules
2. Enable the module
3. Go to admin/settings/contactpage. Here you can:
  - define the location fields you expect; you may need, for example, to set phone to "collect"
  - opt to provide a page, and sepcify the path (e.g. contact)
  - specify whether GPS is being used for the purposes of mapping; this is a requirement from Google
  - specify the dimensions of the map
4. Go to admin/content/contentpage to specify the address
5. Place the address and/or map blocks where you wish, and/or
6. Browse to your new contact page

---------------------------
Overriding the Contact Page
---------------------------

If you wish to change the (rather basic) layout of the contact page, simply copy theme/contactpage-page.tpl.php 
to your theme and modify away.

-------------------
Theming the address
-------------------

The function which generates the address is fully themeable - just implememt theme_contactpage_address(). 


-------------------
Theming the map
-------------------

The function which generates the map is also fully themeable - just implememt theme_contactpage_map().



Lukas White
hello@lukaswhite.com
