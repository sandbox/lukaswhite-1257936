<?php
// This is an extremely simple page incorporating the various elements supported by the module.
// It is anticipated that this be fleshed out in your theme, or simply use the provided blocks instead
?>

<h3>Address</h3>
<?php print theme('contactpage_address', $location) ?>

<h3>Map</h3>
<?php print theme('contactpage_map', $location) ?>